from django.db import models
from django.contrib.auth.models import AbstractUser
from . import manager


# Create your models here.

class User(AbstractUser):
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255, unique=True)
    phone = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    address = models.CharField(max_length=500, null=True, blank=True)
    is_valid = models.BooleanField(default=False)
    otp = models.CharField(max_length=6, null=True, blank=True)
    username = models.CharField(max_length=255, null=True, blank=True)
    # username = None

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = manager.UserManager()