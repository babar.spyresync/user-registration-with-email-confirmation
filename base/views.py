from datetime import datetime, timedelta

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from base.serializers import UserSerializer, VerifyAccountSerializer
from rest_framework.permissions import AllowAny
from .emails import *


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        serializer = UserSerializer(self.user).data
        for k, v in serializer.items():
            data[k] = v
        return data


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class RegisterView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        try:
            data = request.data
            serializer = UserSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                send_otp_via_email(serializer.data['email'])
                return Response({
                    'status': '200',
                    'message': 'User registered successfully Please Check your email for verification',
                    'data': serializer.data
                })
            return Response({
                'status': '400',
                'message': 'User registration failed',
                'data': serializer.errors
            })
        except Exception as e:
            return Response({
                'status': '400',
                'message': 'User registration failed',
                'data': str(e)
            })


class VerifyOTP(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        try:
            data = request.data
            serializer = VerifyAccountSerializer(data=data)
            if serializer.is_valid():
                email = serializer.data['email']
                otp = serializer.data['otp']

                user = User.objects.filter(email=email)
                if not user.exists():
                    return Response({
                        'status': '400',
                        'message': 'email, user not exists',
                        'data': 'invalid email'
                    })
                if user[0].otp != otp:
                    return Response({
                        'status': '400',
                        'message': 'something went wrong',
                        'data': 'invalid OTP'
                    })
                user = user.first()
                user.is_valid = True
                user.save()

                return Response({
                    'status': '200',
                    'message': 'Account verified',
                    'email': serializer.data['email'],
                })
            return Response({
                'status': '400',
                'message': 'User registration failed',
                'data': serializer.errors
            })



        except Exception as e:
            return Response({
                'status': '400',
                'message': 'User registration failed',
                'data': str(e)
            })